  
## APICrypt <a href="https://packagist.org/packages/reezy/api-crypt"><img src="https://img.shields.io/packagist/v/reezy/api-crypt" alt="Latest Version"></a>

请求和响应加解密。  

## 安装依赖 

```bash
composer require reezy/api-crypt
```

## 使用

在需要的控制器或请求上加上中间件 `APICryptMiddleware`
 
 
## 加密解密 


**基本流程**

使用AES加密请求数据，使用RSA加密AES密钥

0. 生成**RSA密钥对(rsaPrivateKey/rsaPublicKey)**，服务端持有私钥，客户端持有公钥 
1. 客户端  
    - 生成 **AES密钥(aesKey)** 并用 **RSA公钥(rsaPublicKey)** 对其加密，得到 **加密的AES密钥(encryptedAesKey)**
    - 客户端使用 **AES密钥(aesKey)** 对 **数据明文(plainData)** 进行加密，得到 **数据密文(encryptedData)** 
    - 将 **加密的AES密钥(encryptedAesKey)** 放在 **请求头(Encryption)**，将 **数据密文(encryptedData)** 作为请求体发送给服务端
2. 服务端
    - 读取请求头，得到 **加密的AES密钥(encryptedAesKey)** 后使用**RSA密钥(rsaPrivateKey)** 解密，得到 **AES密钥(aesKey)** 
    - 读取请求体，得到 **数据密文(encryptedData)** 后使用 **AES密钥(aesKey)** 解密，得到 **数据明文(plainData)**
3. 服务端
    - 处理请求后，使用 **AES密钥(aesKey)** 对响应数据进行加密，返回加密的响应数据
4. 客户端
    - 收到响应后，使用 **AES密钥(aesKey)** 解密


**前后端约定**


- 服务端读取请求头`Encryption: encryptedAesKey;aesIV`存在则表示此请求与其响应需要被加密
  - encryptedAesKey是加密的AES密钥，编码`nowrap base64`
  - aesIV 随机的初始向量，编码 `hex`
  - 客户端只对媒体类型为`application/json`的请求加密，加密后变为`application/octet-stream`，请求体为加密后的数据
  - 响应媒体类型为`application/octet-stream`，解密后为`application/json` 
- RSA公钥(rsaPublicKey)公钥直接存放于客户端(经过一定的混淆)
- AES算法使用 `AES/CBC/PKCS7Padding`，密钥长度256

## 配置

```php
<?php
return [
    // rsa private key
    'private_key' => '',

    'header_name' => 'encryption',
];
```
  

## LICENSE

The Component is open-sourced software licensed under the [Apache license](LICENSE).
